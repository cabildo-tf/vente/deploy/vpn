ARG UBUNTU_VERSION=focal

FROM ubuntu:${UBUNTU_VERSION} as builder

ARG OPENFORTIVPN_VERSION=1.17.1

# hadolint ignore=DL3003,DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        gcc \
        automake \
        autoconf \
        libssl-dev \
        make \
        libc6-dev \
        pkg-config \
        ca-certificates \
        wget && \
    wget -O openfortivpn.tgz --progress=dot:giga https://github.com/adrienverge/openfortivpn/archive/v${OPENFORTIVPN_VERSION}.tar.gz && \
    tar xvzf openfortivpn.tgz && \
    cd "/openfortivpn-${OPENFORTIVPN_VERSION}" && \
    ./autogen.sh && \
    ./configure --prefix=/usr --sysconfdir=/etc && \
    make && \
    make install && \
    rm -rf /var/lib/apt/lists/*


FROM ubuntu:${UBUNTU_VERSION}

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        openssl \
        iproute2 \
        ppp && \
    rm -rf /var/lib/apt/lists/*

COPY /rootfs /

COPY --from=builder /usr/bin/openfortivpn /usr/bin/openfortivpn
