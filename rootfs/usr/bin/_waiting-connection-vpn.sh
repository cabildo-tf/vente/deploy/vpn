#!/bin/bash

RETRIES=60
until [ ${RETRIES} -eq 0 ]
do
	if ip a | grep ppp0 | grep -Pq 'inet\s\d+.\d+.\d+.\d+'
	then
		echo -e "\\n${INFO_COLOR}VPN stabilized${NULL_COLOR}"
		break
	fi

 	echo -ne "."

	RETRIES=$((RETRIES-=1))
	sleep 1
done

if [ ${RETRIES} -eq 0 ]
then
	echo -e "\\n${FAIL_COLOR}Error connected to VPN!${NULL_COLOR}"
	exit 1
fi
