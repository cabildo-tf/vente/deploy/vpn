#!/bin/bash

VPN_PREFIX="FORTI_VPN_"
VPN_PATH_CONFIG="${VPN_PATH_CONFIG:-/vpn}"
VPN_FILE_CONFIG="${VPN_PATH_CONFIG}/vpn.conf"

if [ -z "${FORTI_VPN_HOST}" ]
then
	echo -e "\\n${FAIL_COLOR}You must define 'FORTI_VPN_HOST' in environment${NULL_COLOR}"
	exit 1
fi
mkdir -p "${VPN_PATH_CONFIG}"

# Extrae las variables de entorno para generar el fichero de configuración
env_vars=($(env | grep -e "^${VPN_PREFIX}"))

# Añade registro con la ruta al certificado del usuario
if [ "${USER_CERT}" ]
then
	echo "${USER_CERT}" >> "${VPN_PATH_CONFIG}/user.crt.pem"
	env_vars+=("USER_CERT=${VPN_PATH_CONFIG}/user.crt.pem")
fi

# Añade registro con la ruta a la clave del usuario
if [ "${USER_KEY}" ]
then
	echo "${USER_KEY}" >> "${VPN_PATH_CONFIG}/user.key.pem"
	env_vars+=("USER_KEY=${VPN_PATH_CONFIG}/user.key.pem")
fi

OIFS="${IFS}"

# Crea el fichero de configuración
for var in "${env_vars[@]}"
do
	IFS='=' read -ra ADDR <<< "${var}"
	name=$(echo "${ADDR[0]}" | sed "s/^${VPN_PREFIX}//g" | sed 's/_/-/g' | tr '[:upper:]' '[:lower:]')
	echo "${name} = ${ADDR[1]}" >> "${VPN_FILE_CONFIG}"
done

IFS="${OIFS}"
echo -e "\\n${INFO_COLOR}Connecting to ${DATA_COLOR}${FORTI_VPN_HOST}${NULL_COLOR}"

# Establece la conexión
openfortivpn -c "${VPN_FILE_CONFIG}" > /dev/null 2>&1 &
