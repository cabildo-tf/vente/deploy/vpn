#!/bin/bash

OPENSSL_FILE="/etc/ssl/openssl.cnf"

FIRST_LINE='openssl_conf = default_conf'

read -r -d '' OPENSSL_CONFIG_TO_APPEND << EOM
[default_conf]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
MinProtocol = TLSv1.1
CipherString = DEFAULT@SECLEVEL=1

EOM

ORIGIN_OPENSSL_CONFIG=$(<${OPENSSL_FILE})

echo -e "${FIRST_LINE}\n\n${ORIGIN_OPENSSL_CONFIG}\n\n${OPENSSL_CONFIG_TO_APPEND}" > "${OPENSSL_FILE}"
